# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  answer = 1 + rand(100)
  puts "I have randomly selected a number between 1 and 100."
  print "Guess a number: "
  user_guess = gets.chomp.to_i

  guess_count = 1
  until user_guess.to_i == answer
    guess_count += 1
    print "The number #{user_guess} is too "
    puts user_guess > answer ? "high." : "low."
    print "Try again: "
    user_guess = gets.chomp.to_i
  end

  puts "Congrats! You have guessed the number to be #{answer}."
  print "It only took you #{guess_count} "
  puts guess_count == 1 ? "try!" : "tries."
end #guessing_game

def file_shuffler
  print "I need a file name: "
  user_file = gets.chomp

  from_file = File.readlines(user_file)
  from_file = from_file.map(&:chomp)
  shuffled = from_file.shuffle

  output = File.open("#{user_file}-shuffled.txt", "w")
  shuffled.each {|out| output.puts out}
end #file_shuffler
